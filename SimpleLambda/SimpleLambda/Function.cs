using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

using Amazon.Lambda.Core;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace SimpleLambda
{
    public class Function
    {        
    
        public bool FunctionHandler(ILambdaContext context)
        {
            var isSuccess = false;
            context.Logger.Log("Begin Function");

            var invoiceIdList = new List<int>();

            try
            {
                using (var conn = new SqlConnection(Environment.GetEnvironmentVariable("championProductsDBConnectionString")))
                {
                    using (var Cmd = new SqlCommand($"SELECT InvoiceID FROM Invoice", conn))
                    {
                        conn.Open();

                        SqlDataReader sdr = Cmd.ExecuteReader();

                        while (sdr.Read())
                        {
                            invoiceIdList.Add(Convert.ToInt32(sdr[0]));
                        }

                        conn.Close();
                    }
                }

                using (var conn2 = new SqlConnection(Environment.GetEnvironmentVariable("championProductsDBConnectionString")))
                {
                    foreach (var invoiceId in invoiceIdList)
                    {
                        using (var Cmd = new SqlCommand($"UPDATE dbo.Invoice SET PaidDate = GETDATE() WHERE InvoiceId = " + invoiceId, conn2))
                        {
                            conn2.Open();
                            Cmd.ExecuteNonQuery();
                            conn2.Close();
                        }                        
                    }                    
                }
                isSuccess = true;
            }
            catch (Exception)
            {

                isSuccess = false;
            }

            return isSuccess;
        }
    }
}
